FROM gitpod/workspace-full-vnc

# Install custom tools, runtimes, etc.
# For example "bastet", a command-line tetris clone:
# RUN brew install bastet
#
# More information: https://www.gitpod.io/docs/config-docker/
USER root

ENV BL2_HOME /temp/blockout/

RUN  mkdir -p /temp \
  && wget -qO /temp/bl25-linux-x64.tar.gz "https://sourceforge.net/projects/blockout/files/blockout/BlockOut 2.5/bl25-linux-x64.tar.gz/download" \
  && tar -xzvf /temp/bl25-linux-x64.tar.gz -C /temp 
  

RUN apt-get update \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
  libgl1-mesa-glx \
  libglu1-mesa-dev \
  libxext-dev \
  libsdl1.2-dev \
  libsdl-mixer1.2-dev 
